#include <iostream>

#include "Fib.h"

int main(){

	std::cout << fib(1) << "\n";
	std::cout << fib(2) << "\n";
	std::cout << fib(3) << "\n";
	std::cout << fib(4) << "\n";
	std::cout << fib(5) << "\n";
	std::cout << fib(6) << "\n";

	std::cin.ignore(1, '\n');
    return 0;
}

