#include "Fib.h"

int fib(int n) {

    int next, first = 1, second = 1;

    for (int i = 1; i < n; i++) {
        if (i <= 1) {
            next = i;
        }
        else {
            next = first + second;
            first = second;
            second = next;
        }
    }

    return second;
}
